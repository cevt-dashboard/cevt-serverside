package com.cevt.server.web;


import com.cevt.server.model.Teacher;
import com.cevt.server.model.auth.ERole;
import com.cevt.server.repository.TeachersRepository;
import com.cevt.server.security.payload.request.LoginRequest;
import com.cevt.server.security.payload.request.SignupRequest;
import com.cevt.server.service.AuthService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashSet;
import java.util.Set;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController("AuthController")
@RequestMapping("/api/v1/auth")
@Slf4j
public class AuthController {


    @Autowired
    private AuthService authService;

    @Autowired
    private TeachersRepository teachersRepository;



    @GetMapping(value = "/reset")
    @Hidden
    public Object getAllDenominationMatiere(){
        log.info("Get all Denomination Matiere ");
        Set<String> strRoles = new HashSet<>();
        strRoles.add("ROLE_ADMIN");
        SignupRequest tmp = new SignupRequest();
        tmp.setEmail("admin@email.com");
        tmp.setFirstName("admin");
        tmp.setLastName("admin");
        tmp.setPassword("admin123");
        tmp.setUsername("admin");
        tmp.setRoles(strRoles);
        authService.registerUser(  tmp , ERole.ROLE_ADMIN );

        strRoles = new HashSet<>();
        strRoles.add("ROLE_ENSEIGNANT");
        tmp = new SignupRequest();
        tmp.setEmail("testenseignant@email.com");
        tmp.setFirstName("testenseignant");
        tmp.setLastName("testenseignant");
        tmp.setPassword("testenseignant");
        tmp.setUsername("testenseignant");
        tmp.setRoles(strRoles);
        authService.registerUser(  tmp , ERole.ROLE_ENSEIGNANT );
        return null;
    }

    @PostMapping("/login")
    @Operation(summary = "Retrieve jwt for given user")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {


        return authService.authenticateUser(loginRequest);
    }

    @PostMapping("/create/user")
    @Operation(summary = "signin user to access ressource")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        return authService.registerUser(signUpRequest, ERole.ROLE_USER);
    }

    @PostMapping("/create/admin")
    @Operation(summary = "signin Admin to access ressource")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<?> registerAdmin(@Valid @RequestBody SignupRequest signUpRequest) {
        return authService.registerUser(signUpRequest, ERole.ROLE_ADMIN);
    }

    @PostMapping("/create/enseignant")
    @Operation(summary = "signin Enseignant or Admin to access ressource")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN') ")
    public ResponseEntity<?> registerEnseignant(@Valid @RequestBody SignupRequest signUpRequest) {

        return  authService.registerUser(signUpRequest, ERole.ROLE_ENSEIGNANT);
    }

    @PostMapping("/resetPassword")
    @Operation(summary = "resetPassword")
    public ResponseEntity<?> resetPassword(@Valid @RequestBody SignupRequest signUpRequest) {
        return new ResponseEntity<>("Chaque chose en son temps", HttpStatus.ACCEPTED);
    }

    private void populateDbUsers() {
        // TODO REMOVE

        for(Teacher ee : teachersRepository.findAll()) {
            Set<String> strRoles = new HashSet<>();
            strRoles.add("ROLE_ENSEIGNANT");
            SignupRequest tmp = new SignupRequest();
            tmp.setEmail(ee.getEmail());
            tmp.setFirstName(ee.getFirstName());
            tmp.setLastName(ee.getLastName());
            tmp.setPassword("pass"+ee.getUserId());
            tmp.setUsername(ee.getEmail());
            tmp.setRoles(strRoles);
            System.out.println("EEEEE => "+ tmp);
            authService.registerUser(  tmp , ERole.ROLE_ENSEIGNANT );
        }
    }

}

