package com.cevt.server.web;

import com.cevt.server.model.DenominationMatiere;
import com.cevt.server.repository.DenominationMatiereRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value = "/api/v1/denominations")
@RestController("DenominationController")
@Slf4j
@CrossOrigin
public class DenominationController {

    @Autowired
    private DenominationMatiereRepository denominationMatiereRepository;



    @GetMapping(value = "/all")
    @Deprecated()
    public List<DenominationMatiere> getAllDenominationMatiere(){
        log.info("Get all Denomination Matiere ");

        return denominationMatiereRepository.findAll();
    }

    @PostMapping(value = "/add")
    @Deprecated()
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN') ")
    public DenominationMatiere addDenominationMatiere(@RequestBody DenominationMatiere t) {

        log.info("Add  denominationMatiere " + t.toString());

        return denominationMatiereRepository.save(t);
    }



    @DeleteMapping(value = "/{id}")
    @Deprecated()
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN') ")
    public HttpStatus deleteDenominationMatiere(@PathVariable String id) {

        log.info("delete denominationMatiere " + id );
        try {
            denominationMatiereRepository.deleteById(id);
            return HttpStatus.ACCEPTED;
        }catch (Exception e ){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

}
