package com.cevt.server.web;


import com.cevt.server.model.EvolutionEventTriggered;
import com.cevt.server.repository.EvolutionEventTriggeredRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping(value = "/api/v1/evolutions")
@RestController("EvolutionEventTriggeredController")
@Slf4j
@CrossOrigin

public class EvolutionEventTriggeredController {

    @Autowired
    private EvolutionEventTriggeredRepository evolutionEventTriggeredRepository;

    @GetMapping(value = "")
    @CrossOrigin
    public List<EvolutionEventTriggered> getAllEvolutionEvent(){
        log.info("Get all Denomination Matiere ");

        return evolutionEventTriggeredRepository.findAll();
    }

    @GetMapping(value = "/date/{date}")// String pattern = "yyyy-MM-dd";
    @CrossOrigin
    public List<EvolutionEventTriggered> getAllEvolutionEventByDate(@PathVariable String date){
        log.info("Get all Denomination Matiere ");

        return evolutionEventTriggeredRepository.findByDate(date) ;
    }


}
