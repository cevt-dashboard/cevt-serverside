package com.cevt.server.web;

import com.cevt.server.model.presence.Presence;
import com.cevt.server.service.presence.PresenceService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RequestMapping(value = "/api/v1/presence")
@RestController("PresenceController")
@Slf4j
@CrossOrigin
public class PresenceController {
    @Autowired
    private PresenceService presenceService;

    @GetMapping(value = "/create/{enseignant}/{seanceId}/{filiere}")
    @Operation(summary = "  ")
    public Presence createPresence(@PathVariable String enseignant, @PathVariable String seanceId, @PathVariable String filiere ) {

        return presenceService.startWorkflow(enseignant, seanceId, filiere);
    }


    @GetMapping(value = "/update/{code}/{name}/{fingerprint}")
    @Operation(summary = " ")

    public ResponseEntity<?> addCustomEvents(@PathVariable String code, @PathVariable String name, @PathVariable String fingerprint) {
        log.info("Presence " + code + name + fingerprint);
        boolean response = false;
        try {
              response =   presenceService.update(code, name, fingerprint);
        } catch ( Exception e ) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/close/{code}")
    @Operation(summary = " ")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN') ")
    public ResponseEntity<?> closeCustomEvents(@PathVariable String code) {

        boolean response = false;
        try {
            response =   presenceService.close(code);
        } catch ( Exception e ) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(value = "/{code}")
    @Operation(summary = "  ")
    public Presence findPresence(@PathVariable String code  ) {

        return presenceService.presenceRepository.findByCode(code) ;
    }

    @GetMapping(value = "/all/{enseignant}")
    @Operation(summary = "  ")
    public List<Presence> findAllPresence(@PathVariable String enseignant ) {

        return presenceService.presenceRepository.findByEnseignant(enseignant) ;
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN') ")
    public HttpStatus deletePresenceMatiere(@PathVariable String id) {

        log.info("deletePresenceMatiere " + id );
        try {
            presenceService.presenceRepository.deleteById(id);
            return HttpStatus.ACCEPTED;
        }catch (Exception e ){
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }

}
