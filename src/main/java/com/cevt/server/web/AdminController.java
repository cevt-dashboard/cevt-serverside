package com.cevt.server.web;

import com.cevt.server.model.CalendarEvents;
import com.cevt.server.model.EventType;
import com.cevt.server.repository.CalendarEventsRepository;
import com.cevt.server.service.CalendarEventsService;
import com.cevt.server.service.publication.SchedulerService;
import com.cevt.server.service.publication.SnitchService;
import io.swagger.v3.oas.annotations.Hidden;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import twitter4j.Twitter;
import twitter4j.TwitterException;

import java.util.List;

@RequestMapping(value = "/admin")
@RestController("AdminController")
@Slf4j
@CrossOrigin
public class AdminController {

    @Autowired
    private SchedulerService schedulerService;

    @Autowired
    private SnitchService snitchService;
    @Autowired
    private Twitter twitter;

    @Autowired
    private CalendarEventsService calendarEventsService;

    @Autowired
    private CalendarEventsRepository calendarEventsRepository;

    @GetMapping(value = "/triggerPublication")
    @Deprecated
    @Hidden
    @PreAuthorize("hasRole('ADMIN')")
    public Object getGroupEvents() {

        log.info("Manual Trigger ");
        // [ icsetudiant/m2miaa.ics    ,    m2miaa.ics
        return this.schedulerService.notifyGroupEventsChanges() ;
    }




    @PutMapping(value = "/reload")
    @Operation(summary = "dump all ")
    @PreAuthorize("hasRole('ADMIN')")
    public int reload( ) {

        log.info("Getting teachersEvents ");

        return calendarEventsService.dumpTeachersData(true).size();
    }

    @GetMapping(value = "/pending/events")
    @PreAuthorize("hasRole('ADMIN')")
    public List<CalendarEvents> GetPendingEvents(@RequestParam EventType valid) throws TwitterException {


        return calendarEventsRepository.findAllByValidAndCustom(valid, EventType.YES );
    }



    @PostMapping(value = "/add")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpStatus addCustomEvents(@RequestBody CalendarEvents event) {
        log.info("Add  Group ");
        try {
            event.setCustom(EventType.YES);
            calendarEventsRepository.save(event);

        } catch ( Exception e ) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "delete all events ")
    @PreAuthorize("hasRole('ADMIN')")
    public HttpStatus deleteAllEvents(@PathVariable String id ){



        try {
            calendarEventsRepository.deleteById(id);

        } catch ( Exception e ) {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return HttpStatus.OK;
    }


    @DeleteMapping(value = "/all")
    @Operation(summary = "delete all events ")
    @PreAuthorize("hasRole('ADMIN')")
    public long deleteAllEvents( ){

        return  calendarEventsRepository.deleteAllByCustom(EventType.NO);
    }




}
