package com.cevt.server.web;

import com.cevt.server.model.CalendarEvents;
import com.cevt.server.model.EventType;
import com.cevt.server.repository.CalendarEventsRepository;
import com.cevt.server.service.CalendarEventsService;
import com.cevt.server.utils.CustomFunctions;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RequestMapping(value = "/api/v1/events")
@RestController("EventsController")
@Slf4j
@CrossOrigin

public class EventsController {

    @Autowired
    private CalendarEventsService calendarEventsService;

    @Autowired
    private CalendarEventsRepository calendarEventsRepository;

    @Autowired
    private CustomFunctions customFunctions;


    @GetMapping(value = "/paginated")
    @Operation(summary = "Get Events Paginated")
    public Page<CalendarEvents> all(@RequestParam int page, @RequestParam int size) {

        log.info("Getting teachersEvents " );

        return calendarEventsRepository.findAll(PageRequest.of(page, size));
    }

    @GetMapping(value = "/find/{id}")
    @Operation(summary = "Given Teacher  -> return list of events ")
    public Optional<CalendarEvents> getTeacherEvents(@PathVariable String id )   {

        log.info("Find events by id : "+ id  );

        return calendarEventsRepository.findById(id);
    }

    @GetMapping(value = "/findmyevents/{creatorName}")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN')")
    @Operation(summary = "Given Teacher  -> return list of events ")
    public List<CalendarEvents> getOwnTeacherEvents(@PathVariable String creatorName )   {

        log.info("Find events by id : "+ creatorName  );

        return calendarEventsRepository.findAllByCreator(creatorName) ;
    }


    @GetMapping(value = "/{firstName}/{lastName}")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN')")
    @Operation(summary = "Given Teacher  -> return list of events ")
    public List<CalendarEvents> getTeacherEvents(@PathVariable String firstName,
                                                 @PathVariable String lastName )  {

        log.info("Getting teachersEvents "+ firstName + " " + lastName );

        return calendarEventsRepository.findAllByProfIgnoreCaseAndValid(
                customFunctions.getTeachersCalName(firstName, lastName), EventType.YES );
    }

    @GetMapping(value = "/pending")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN')")
    public List<CalendarEvents> GetProfPendingEvents(@RequestParam EventType valid, @RequestParam String nomProf ) {

        return calendarEventsRepository.findAllByProfIgnoreCaseAndValid(nomProf, valid);
    }

    @GetMapping(value = "/filieres")
    @Operation(summary = " Return all classe names availables (with their variants also) ")
    public List<String> getTeacherCategories( )  {

        return  calendarEventsService.queryAllCategory();
    }

    @GetMapping(value = "/teachers/{filiereName}")
    @Operation(summary = " Return all teachers of specific filiere ")
    public List<String> getAllTeachersOfFiliere(@PathVariable String filiereName )  {

        return  calendarEventsService.queryAllEnseignantOfFiliere(filiereName.toUpperCase());
    }


    @GetMapping(value = "/teacher/{matiereName}")
    @Operation(summary = " ")
    public List<String> getEnseignantOfMatiere(@PathVariable String matiereName) {
        // NOT QUIT DONE YET, BUT WORKS
        return  calendarEventsService.queryEnseignantsOfMatiere(matiereName.toUpperCase());
    }

    @GetMapping(value = "/teacher/{firstName}/{lastName}")
    @Operation(summary = " ")
    public List<String> queryALLMatiereOfEnseignant(@PathVariable String firstName, @PathVariable String lastName ) {
        // NOT QUIT DONE YET, BUT WORKS
        return  calendarEventsService.queryALLMatiereOfEnseignant(firstName, lastName );
    }


    @GetMapping(value = "/matieres")
    @Operation(summary = " return all matières available ")
    public List<String> getMatieres() {
        List<String> matieres = calendarEventsService.queryAllMatiere();
        Set<String>  set = new HashSet<>(matieres);
        matieres.clear();
        matieres.addAll(set);

        matieres.sort( Comparator.comparing( String::toString ) );

        return matieres;
    }

    @GetMapping(value = "/{filiereName}/matiere")
    @Operation(summary = "   ")
    public List<String> getMatiereOfFiliere(@PathVariable String filiereName) {

        return  calendarEventsService.queryAllMatiereOfFiliere(filiereName.toUpperCase());
    }


    @GetMapping(value = "/{groupName}")
    @Operation(summary = "Given groupName -> return list of events ")
    public List<CalendarEvents> getGroupEvents(@PathVariable String groupName ) throws IOException {

        return  calendarEventsRepository.findAllByCategoriesContainingIgnoreCase(groupName);
    }


    @PostMapping(value = "/add")
    @Operation(summary = "Add Custom Event")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN') ")
    public ResponseEntity<?> addCustomEvents(@Valid @RequestBody CalendarEvents event) {
        log.info("Add Group ");
        CalendarEvents response = null;
        try {
            event.setCustom(EventType.YES);
            event.setValid(EventType.NO);
            response =  calendarEventsRepository.save(event);
        } catch ( Exception e ) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @DeleteMapping(value= "/{id}")
    @Operation(summary = "delete events from db")
    @PreAuthorize("hasRole('ENSEIGNANT') or hasRole('ADMIN') ")
    public ResponseEntity<?> deleteEventsNotValid(@PathVariable(value= "id") String id) {
        log.info("deleting event :  " + id);
        CalendarEvents event = calendarEventsRepository.findById(id).get();
        if( event != null && (  event.getCustom().equals(EventType.NO)) ) {

            return new ResponseEntity<>("User not authorized to do such action", HttpStatus.UNAUTHORIZED);

        }
        long val =  calendarEventsRepository.deleteAllById(id);

        if ( val == 1) {
            return new ResponseEntity<>("Deleted Successfully ", HttpStatus.OK);
        }
        if( val == 0 ) {
            return new ResponseEntity<>("Cannot Find Id : " + id, HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>("Internal Server Error", HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
