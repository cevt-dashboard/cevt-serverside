package com.cevt.server.web;

import com.cevt.server.model.CalendarEvents;
import com.cevt.server.model.stats.Average;
import com.cevt.server.model.stats.MonthlyDetailsParMatiere;
import com.cevt.server.model.stats.Statsdata;
import com.cevt.server.model.stats.TotalPerMonth;
import com.cevt.server.service.StatService;
import com.cevt.server.utils.CustomFunctions;
import io.swagger.v3.oas.annotations.Operation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;


@RequestMapping(value = "/api/v1/stats")
@RestController("StatController")
@Slf4j
@CrossOrigin
public class StatController {

    @Autowired
    private StatService statService;
    @Autowired
    private CustomFunctions customFunctions;

    @GetMapping(value = "/annual-averages")
    @Operation(summary = "Return averages (global) calculated with all teachers present in the Teacher-DB, pattern = \"yyyy-MM-dd\"")
    public Average getAnnualAverage(  @RequestParam( value = "start_date", required = false)
                                      @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                      @RequestParam( value = "end_date", required = false)
                                      @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate )
            throws ParseException
    {

        log.info("Getting annual average");
        return statService.getAnnualAverage(startDate, endDate);
    }

    @GetMapping(value = "/monthly-total")
    @Operation(summary = "Return averages PER month calculated with all teachers present in the Teacher-DB, pattern = \"yyyy-MM-dd\"")
    public TotalPerMonth getMonthlyAverage(
                                        @RequestParam( value = "date", required = false)
                                             @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
                                        @RequestParam( value = "start_date", required = false)
                                             @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                        @RequestParam( value = "end_date", required = false)
                                            @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate )
            throws ParseException
    {

        log.info("Getting monthly average per month");
        return statService.getTotalPerMonth(startDate, endDate, date );

    }

    @GetMapping(value = "/teacher-total/{firstname}/{lastname}")
    @Deprecated
    @Operation(summary = "Return Total stats teacher should do (cm/td/tp) && pattern = \"yyyy-MM-dd\" ")
    public Statsdata getGeneralDataPerTeacher(@PathVariable String firstname,
                                              @PathVariable String lastname ,
                                              @RequestParam( value = "start_date", required = false)
                                                  @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                              @RequestParam( value = "end_date", required = false)
                                                  @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate )
            throws ParseException
    {

        log.info("Getting one teacher-average average"+ firstname + " "+ lastname);

        return statService.getGeneralDataPerTeacher(firstname, lastname, startDate, endDate );
    }

    @GetMapping(value = "/teacher/matieres/{firstname}/{lastname}")
    @Operation(summary = "Return Total stats teacher should do (cm/td/tp) && pattern = \"yyyy-MM-dd\" ")
    public Map<String, MonthlyDetailsParMatiere> getGeneralDataPerTeacherPerMatiere(@PathVariable String firstname,
                                                                                    @PathVariable String lastname ,
                                                                                    @RequestParam( value = "start_date", required = false)
                                              @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                                                    @RequestParam( value = "end_date", required = false)
                                              @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate )
            throws ParseException
    {

        log.info("Getting one teacher-average average"+ firstname + " "+ lastname);

        return statService.getGeneralDataPerTeacherPerMatiere(firstname, lastname, startDate, endDate );
    }

    @GetMapping(value = "/teacher/details/{firstname}/{lastname}")
    @Operation(summary = "Return Total + Done (cm/td/tp as of today) and Montly-Stats for given teacher")
    public Map<String, Object> getSpecificDetails(@PathVariable String firstname, @PathVariable String lastname,
                                                  @RequestParam( value = "date", required = false)
                                                    @DateTimeFormat(pattern = "yyyy-MM-dd") Date date,
                                                  @RequestParam( value = "start_date", required = false)
                                                      @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                  @RequestParam( value = "end_date", required = false)
                                                      @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate)
            throws ParseException
    {

        log.info("Getting details specific per teacher " + firstname + " "+ lastname);

        return statService.getStatsSpecific(
                        customFunctions.getTeachersCalName(firstname, lastname), date, startDate, endDate );
    }


    @GetMapping(value = "/teacher/events-grouped-by-categories/{firstname}/{lastname}")
    @Operation(summary = "Return all event for teacher grouped by categories ")
    public Map<String, List<CalendarEvents>> getEventByGroupTeacher(
            @PathVariable String firstname, @PathVariable String lastname,
            @RequestParam( value = "start_date", required = false)
            @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @RequestParam( value = "end_date", required = false)
            @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate)
            throws ParseException
    {

        log.info("Getting charges-by-group "+ firstname + " "+ lastname);

        return statService.getEventsByGroupTeacher(firstname, lastname, startDate, endDate);
    }


    @GetMapping(value = "/teacher/stats-by-group/{firstname}/{lastname}/{groupName}")
    @Operation(summary = "Return stats (cm/tp/td) for teacher grouped by unique group Name ")
    public Map<String, Statsdata> getStatsByGroupTeacher(@PathVariable String firstname,
                                                         @PathVariable String lastname,
                                                         @PathVariable String groupName,
                                                         @RequestParam( value = "start_date", required = false)
                                                            @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
                                                         @RequestParam( value = "end_date", required = false)
                                                             @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate)
            throws ParseException
    {

        log.info("Getting stats-by-group "+ firstname + " "+ lastname);
        return statService.getStatsDataForTeacherByGroups(
                customFunctions.getTeachersCalName(firstname, lastname), groupName, startDate, endDate);
    }
}
