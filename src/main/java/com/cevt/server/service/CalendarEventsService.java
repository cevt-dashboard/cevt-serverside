package com.cevt.server.service;

import com.cevt.server.model.CalendarEvents;
import com.cevt.server.model.EventDescription;
import com.cevt.server.model.EventType;
import com.cevt.server.model.EvolutionEventTriggered;
import com.cevt.server.model.Teacher;
import com.cevt.server.repository.CalendarEventsRepository;
import com.cevt.server.repository.EvolutionEventTriggeredRepository;
import com.cevt.server.repository.TeachersRepository;
import com.cevt.server.utils.CustomFunctions;
import com.cevt.server.utils.ICSKeys;
import com.mongodb.client.DistinctIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Filters;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;

@Component
@Slf4j
@EnableAsync
public class CalendarEventsService  extends AbstractService {


    @Autowired
    private CalendarEventsRepository calendarEventsRepository;

    @Autowired
    private TeachersRepository teachersRepository;

    @Autowired
    private DataService dataService;

    @Autowired
    private CustomFunctions customFunctions;

    @Autowired
    MongoTemplate mongoTemplate;

    @Autowired
    EvolutionEventTriggeredRepository evolutionEventTriggeredRepository;


    public List<CalendarEvents> dumpTeachersData(boolean save) {

        final List<CalendarEvents> oldEvents = calendarEventsRepository.findAllByCustom(EventType.NO);
        if(save) {

            calendarEventsRepository.deleteAllByCustom(EventType.NO);
        }

        List<CalendarEvents> arrayEvents = new ArrayList<>();

        String calName = "";
        String uri = "";

        for (Teacher teacher : teachersRepository.findAll()) {

            try {
                calName = customFunctions.getTeachersCalName(teacher.getFirstName(), teacher.getLastName());
                uri = customFunctions.getTeachersURL(teacher.getLastName() + "_" + teacher.getFirstName());
                arrayEvents.addAll(teachersEventsAsPojo(dataService.getEventsAsArray(uri), calName) );
                log.info("Loading successful for : " + uri);

            } catch (Exception e) {
                log.info("Cannot find retrieve from vtAgenda for : " + uri);
            }
        }

        if(save){
            calendarEventsRepository.saveAll(arrayEvents);

            Executors.newCachedThreadPool().submit(() -> {
                EvolutionEventTriggered tmp = filterDocuments(oldEvents, arrayEvents);
                System.out.println("EXUCTORSSS => "+ tmp);
                evolutionEventTriggeredRepository
                        .insert(tmp);
                return null;
            });

        }

        return arrayEvents;
    }

    private EvolutionEventTriggered filterDocuments(List<CalendarEvents> oldEvents, List<CalendarEvents> newEvents) {

        List<CalendarEvents> res_old = new ArrayList<>();
        List<CalendarEvents> res_new = new ArrayList<>();

        for(int i=0; i< oldEvents.size(); i++) {
            for(int j=0; j< newEvents.size(); j++) {
                if( oldEvents.get(i).equals(newEvents.get(j)) ) {

                    if( oldEvents.get(i).hasChanged(newEvents.get(j) )  ) {
                        res_old.add(oldEvents.get(i));
                        res_new.add(newEvents.get(j));
                        j = newEvents.size() -1;
                    }
                }
            }
        }
        EvolutionEventTriggered tmp = new EvolutionEventTriggered(res_old, res_new, new SimpleDateFormat("yyyy-MM-dd")
               .format(new Date()));

        return tmp;
    }

    public List<CalendarEvents> teachersEventsAsPojo(List<String> array, String calName) {

        List<CalendarEvents> eventList = new ArrayList<>();
        CalendarEvents e = new CalendarEvents();

        for(String val: array) {

            if(val.contains(ICSKeys.BEGIN.getDescription())) {
                e = new CalendarEvents();
                e.setCustom(EventType.NO);
                e.setValid(EventType.YES);
                e.setProf(calName);
                e.setCreator("VtAgenda");
            }

            if( val.contains(ICSKeys.SUMMARY.getDescription())) {
                e.setSummary(val.replaceAll(ICSKeys.SUMMARY.getDescription(), ""));
            }

            if(val.contains(ICSKeys.CATEGORIES.getDescription())) {
                e.setCategories(filterFiliereToBeConform (
                        val.replaceAll(ICSKeys.CATEGORIES.getDescription(), "") )
                );
            }

            if( val.contains(ICSKeys.DTSTART.getDescription()) ) {
                e.setDtStart( getDateFromString (val.replaceAll(ICSKeys.DTSTART.getDescription(), "")));
            }

            if(val.contains(ICSKeys.DTEND.getDescription())) {
                e.setDtEnd(getDateFromString (val.replaceAll(ICSKeys.DTEND.getDescription(), "")) );
            }

            if( val.contains(ICSKeys.LOCATION.getDescription())) {
                e.setLocation(val.replaceAll(ICSKeys.LOCATION.getDescription(), ""));
            }

            if( val.contains(ICSKeys.DESCRIPTION.getDescription())) {

                    EventDescription desc = getTeacherDescription(val, e.getSummary() );
                    e.setLanguage(desc.getLanguage());
                    e.setMatiere( trimMatiereToBeConform(desc.getMatiere()) );
                    e.setType(desc.getType());
                    e.setGroupe(desc.getGroupe());
                    e.setDuree(desc.getDuree());

            }

            if( val.contains(ICSKeys.DTSTAMP.getDescription())) {
                e.setDtStamp( getDateFromString (val.replaceAll(ICSKeys.DTSTAMP.getDescription(), "")) );
            }
            if(val.contains(ICSKeys.UID.getDescription())) {
                e.setUid(val.replaceAll(ICSKeys.UID.getDescription(), ""));
            }

            if(val.contains(ICSKeys.CALNAME.getDescription())) {
                e.setProf(val.replaceAll(ICSKeys.CALNAME.getDescription(), "").toUpperCase() );
            }

            if(val.contains(ICSKeys.END.getDescription())) {
                eventList.add(e);
            }

        }

        return  eventList;

    }


    public List<String> queryAllCategory() {
        List<String> categoryList = new ArrayList<>();
        MongoCollection mongoCollection = mongoTemplate.getCollection("CalendarEvents");
        DistinctIterable distinctIterable = mongoCollection.distinct("categories", String.class);
        MongoCursor cursor = distinctIterable.iterator();
        while (cursor.hasNext()) {
            String category = (String)cursor.next();
            categoryList.add(category);
        }
        return categoryList;
    }

    public List<String> queryAllMatiere() {


        MongoCursor cursor = mongoTemplate.getCollection("CalendarEvents")
                                    .distinct("matiere", String.class)
                                    .iterator();

        List<String> categoryList = new ArrayList<>();
        while (cursor.hasNext()) {
            String category = (String)cursor.next();
            categoryList.add(category);
        }
        return categoryList;
    }

    public List<String> queryAllMatiereOfFiliere(String filiereName) {

        MongoCursor<String> cursor =
                mongoTemplate.getCollection("CalendarEvents")
                        .distinct("matiere", String.class)
                        .filter(Filters.regex("categories",  filiereName))
                        .iterator();

        List<String> categoryList = new ArrayList<>();
        while (cursor.hasNext()) {
            String category = (String)cursor.next();
            categoryList.add(category);
        }
        return categoryList;


    }


    public List<String> queryAllEnseignantOfFiliere(String filiereName) {

        MongoCursor<String> cursor =
                mongoTemplate.getCollection("CalendarEvents")
                        .distinct("prof", String.class)
                        .filter(Filters.regex("categories",  filiereName))
                        .iterator();

        List<String> categoryList = new ArrayList<>();
        while (cursor.hasNext()) {
            String category = (String)cursor.next();
            categoryList.add(category);
        }
        return categoryList;
    }

    public List<String> queryEnseignantsOfMatiere(String matiereName) {

        MongoCursor<String> cursor =
                mongoTemplate.getCollection("CalendarEvents")
                        .distinct("prof", String.class)
                        .filter(Filters.eq("matiere",  matiereName))
                        .iterator();

        List<String> categoryList = new ArrayList<>();
        while (cursor.hasNext()) {
            String category = (String)cursor.next();
            categoryList.add(category);
        }
        return categoryList;
    }


    public List<String> queryALLMatiereOfEnseignant(String firstName, String lastName) {

        MongoCursor<String> cursor =
                mongoTemplate.getCollection("CalendarEvents")
                        .distinct("matiere", String.class)
                        .filter(Filters.eq("prof",  customFunctions.getTeachersCalName(firstName, lastName)))
                        .iterator();

        List<String> categoryList = new ArrayList<>();
        while (cursor.hasNext()) {
            String category = (String)cursor.next();
            categoryList.add(category);
        }
        return categoryList;
    }
}
