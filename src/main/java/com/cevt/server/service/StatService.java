package com.cevt.server.service;

import com.cevt.server.model.CalendarEvents;
import com.cevt.server.model.EventType;
import com.cevt.server.model.Teacher;
import com.cevt.server.model.stats.Average;
import com.cevt.server.model.stats.AverageDetails;
import com.cevt.server.model.stats.MonthlyDetails;
import com.cevt.server.model.stats.MonthlyDetailsParMatiere;
import com.cevt.server.model.stats.Statsdata;
import com.cevt.server.model.stats.TotalPerMonth;
import com.cevt.server.model.stats.TotalStats;
import com.cevt.server.repository.CalendarEventsRepository;
import com.cevt.server.repository.TeachersRepository;
import com.cevt.server.utils.CustomFunctions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/*
This class is used to study the database and retrieve informations about teachers :
- getAnnualAverage -> By default, arguments are null and the function return total annual average of cm/tp/td
- getTotalPerMonth -> same as getAnnualAverage but with more details, monthly
- getGeneralDataPerTeacher -> get cm/td/tp  DONE for one teacher
- getStatsSpecific -> heavy process but return DONE + TOTAL + details for one teacher (tp/td/cm )
- getEventsByGroupTeacher -> given a teacher, aggregate his classes and there events
- getStatsDataForTeacherByGroups -> display how much teacher has to teach a specific class
 */

@Component
@Slf4j
public class StatService extends AbstractService {


    private final static String TD = "TD";
    private final static String TP = "TP";
    private final static String CM = "CM";

    @Autowired
    private CustomFunctions customFunctions;

    @Autowired
    private CalendarEventsRepository eventRepository;

    @Autowired
    private TeachersRepository teachersRepository;

    public Average getAnnualAverage(Date startDate, Date endDate) throws ParseException {

        // TODO Add interval Date to Repository request
        // TODO improve later
        if(startDate == null || endDate == null) {
            startDate = customFunctions.getCoupleDateStartAndEnd().getKey();
            endDate = customFunctions.getCoupleDateStartAndEnd().getValue();
        }

        Statsdata data = new Statsdata();
        Set<String> dataProviders = new TreeSet<>();
        List<CalendarEvents> eventList =  eventRepository
                .findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndValid(startDate, endDate, EventType.YES);
        for(CalendarEvents vEventTeachers : eventList) {
            switch (vEventTeachers.getType()) {
                case TD:
                    data.setTd( customFunctions.calculateWeigthHours(vEventTeachers.getDuree() )   );
                case TP:
                    data.setTp( customFunctions.calculateWeigthHours(vEventTeachers.getDuree() )   );
                default:
                    data.setCm( customFunctions.calculateWeigthHours(vEventTeachers.getDuree() )   );
            }
            dataProviders.add(vEventTeachers.getProf());
        }

        List<AverageDetails> averageDetailsList = new ArrayList<>();
        averageDetailsList.add(new AverageDetails(CM, "Hours", data.getCm(), data.getCm()/dataProviders.size()));
        averageDetailsList.add(new AverageDetails(TD, "Hours", data.getTd(),  data.getTd()/dataProviders.size()));
        averageDetailsList.add(new AverageDetails(TP, "Hours",  data.getTp(),  data.getTp()/dataProviders.size()));

        return new Average(averageDetailsList, dataProviders);

    }

    public TotalPerMonth getTotalPerMonth(Date startDate, Date endDate, Date date) throws ParseException {

        TotalPerMonth res = new TotalPerMonth();
        List<TotalStats> stats = new ArrayList<>();
        String calName = "";
        Map<Integer, MonthlyDetails> infos = new HashMap<>();
        Set<String> dataProviders = new HashSet<>();
        if(date == null) {
            date = new Date();
        }
        if(startDate == null || endDate == null) {
            startDate = customFunctions.getCoupleDateStartAndEnd().getKey();
            endDate = customFunctions.getCoupleDateStartAndEnd().getValue();
        }

        for(Teacher t: teachersRepository.findAll()) {
            calName = customFunctions.getTeachersCalName(t.getFirstName(), t.getLastName() );
            try {
                TotalStats specificStats = (TotalStats) this.getStatsSpecific( calName, date, startDate, endDate ).get("Stats");
                if( specificStats.getInfos().size() != 0 ){
                    stats.add(specificStats);
                    dataProviders.add(calName);
                }
            }catch (Exception e) {
                log.info(e.getMessage());
            }
        }


        for(TotalStats t: stats) {
            for (Integer key : t.getInfos().keySet() ) {
                if( infos.keySet().contains(key) ) {
                    infos.get(key).setAll(t.getInfos().get(key));
                } else {
                    MonthlyDetails trash = new MonthlyDetails();
                    trash.setAll(t.getInfos().get(key));
                    infos.put(key, trash);
                }
            }
        }



        res.setMap(infos);
        res.setDataProviders(dataProviders);

        return res;
    }

    public Statsdata getGeneralDataPerTeacher(String firstName, String lastName, Date start, Date end)
            throws ParseException {
        if(start == null || end == null) {
            start = customFunctions.getCoupleDateStartAndEnd().getKey();
            end = customFunctions.getCoupleDateStartAndEnd().getValue();
        }
        Statsdata data = new Statsdata();
        for(CalendarEvents vEventTeachers : eventRepository
                .findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndProfAndValid(
                         start, end, customFunctions.getTeachersCalName(firstName, lastName) , EventType.YES) ) {

            switch (vEventTeachers.getType()) {
                case TD:
                    data.setTd( customFunctions.calculateWeigthHours(vEventTeachers.getDuree() ) );
                case TP:
                    data.setTp( customFunctions.calculateWeigthHours(vEventTeachers.getDuree() ) );
                default:
                    data.setCm( customFunctions.calculateWeigthHours(vEventTeachers.getDuree() ) );
            }

        }
        return data;
    }

    public Map<String, MonthlyDetailsParMatiere> getGeneralDataPerTeacherPerMatiere(String firstName, String lastName, Date start, Date end)
            throws ParseException {
        if(start == null || end == null) {
            start = customFunctions.getCoupleDateStartAndEnd().getKey();
            end = customFunctions.getCoupleDateStartAndEnd().getValue();
        }
        MonthlyDetailsParMatiere data = new MonthlyDetailsParMatiere();
        Map<String, MonthlyDetailsParMatiere> mapResult = new HashMap<>();
        Date today = new Date();
        List<CalendarEvents> eventsList = eventRepository
                .findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndProfAndValid(
                        start, end, customFunctions.getTeachersCalName(firstName, lastName) , EventType.YES);

        for(CalendarEvents vEventTeachers : eventsList ) {

            data = new MonthlyDetailsParMatiere();
            switch (vEventTeachers.getType()) {
                case TD:
                    if(vEventTeachers.getDtStart().before(today)) {
                        data.setTdDone((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));
                    }
                    data.setTdTotal((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));


                case TP:
                    if(vEventTeachers.getDtStart().before(today)) {
                        data.setTpDone((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));
                    }
                        data.setTpTotal((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));


                default:
                    if(vEventTeachers.getDtStart().before(today)) {
                        data.setCmDone((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));
                    }
                        data.setCmTotal((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));

            }

            if(mapResult.get(vEventTeachers.getMatiere()) == null) {
                data.setDate_debut(vEventTeachers.getDtStart());
                data.setDate_fin(vEventTeachers.getDtStart());
                mapResult.put(vEventTeachers.getMatiere(), data );
            }else {

                mapResult.get(vEventTeachers.getMatiere()).setDate_fin(vEventTeachers.getDtStart());
                mapResult.get(vEventTeachers.getMatiere()).setAll(data);
            }


        }
        return mapResult;
    }

    public Map<String, Object> getStatsSpecific(String calName,
                                                Date date,
                                                Date start,
                                                Date end) throws ParseException {
        if(date == null) {
            date = new Date();
        }
        if(start == null || end == null) {
            start = customFunctions.getCoupleDateStartAndEnd().getKey();
            end = customFunctions.getCoupleDateStartAndEnd().getValue();
        }
        Statsdata done = new Statsdata();
        Statsdata total = new Statsdata();
        TotalStats totalStats = new TotalStats();
        Map<String, Object> map = new HashMap<>();
        totalStats.setCalName(calName);
        List<CalendarEvents> eventList = eventRepository
                .findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndProfAndValid(start, end, calName, EventType.YES);
        Set<Integer> labels = new TreeSet<Integer>();

        for(CalendarEvents vEventTeachers : eventList) {
            labels.add(labelBuilder(vEventTeachers));
        }

        for(Integer mois: labels ) {
            MonthlyDetails details = new MonthlyDetails();
            totalStats.getInfos().put(mois, details);
        }

        for(CalendarEvents vEventTeachers : eventList) {
            switchCase(vEventTeachers, totalStats, labelBuilder(vEventTeachers), date );
        }

        for(MonthlyDetails t: totalStats.getInfos().values()) {
            done.setCm(t.getCmDone());
            done.setTd(t.getTdDone());
            done.setTp(t.getTpDone());
            total.setCm(t.getCmTotal());
            total.setTd(t.getTdTotal());
            total.setTp(t.getTpTotal());
        }
        map.put("Done", done);
        map.put("Total", total);
        map.put("Stats", totalStats);

        return map;
    }

    public Map<String, List<CalendarEvents>> getEventsByGroupTeacher(String firstName,
                                                                     String lastName,
                                                                     Date start,
                                                                     Date end) throws ParseException {
        if(start == null || end == null) {
            start = customFunctions.getCoupleDateStartAndEnd().getKey();
            end = customFunctions.getCoupleDateStartAndEnd().getValue();
        }

        Map<String, List<CalendarEvents>> eventDescriptions = new HashMap<>();
        for(CalendarEvents event: eventRepository.findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndProfAndValid(
                start, end,  customFunctions.getTeachersCalName(firstName, lastName) , EventType.YES  )) {


            if(eventDescriptions.keySet().contains( event.getCategories() )) {
                eventDescriptions.get(event.getCategories()).add(event);
            } else {
                List<CalendarEvents> listEvent = new ArrayList<>();
                listEvent.add(event);
                eventDescriptions.put( event.getCategories() ,  listEvent );
            }
        }

        return eventDescriptions;

    }

    public Map<String, Statsdata> getStatsDataForTeacherByGroups(String calName, String groupName,
                                                                 Date start,
                                                                 Date end) throws ParseException {
        if(start == null || end == null) {
            start = customFunctions.getCoupleDateStartAndEnd().getKey();
            end = customFunctions.getCoupleDateStartAndEnd().getValue();
        }

        List<CalendarEvents> eventList = eventRepository
                .findAllByProfAndCategoriesContainingIgnoreCaseAndDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndValid
                        (calName, groupName, start, end, EventType.YES);

        Map<String, Statsdata> statsdataMap = new HashMap<>();

        Statsdata data = new Statsdata();
        for(CalendarEvents event : eventList ) {
            switch ( event.getType()) {
                case TD:
                    data.setTd( customFunctions.calculateWeigthHours(event.getDuree() )   );
                case TP:
                    data.setTp( customFunctions.calculateWeigthHours(event.getDuree() )   );
                default:
                    data.setCm( customFunctions.calculateWeigthHours(event.getDuree() )   );
            }
            statsdataMap.put(groupName, data);
        }


        return statsdataMap;

    }


    private TotalStats switchCase(CalendarEvents vEventTeachers, TotalStats totalStats, int labels, Date date){


        switch (vEventTeachers.getType()) {
            case "TD":
                totalStats.getInfos().get(labels)
                        .setTdTotal((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));
                if(vEventTeachers.getDtStart().before(date)) {
                    totalStats.getInfos().get(labels)
                            .setTdDone((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));
                }

            case "TP":
                totalStats.getInfos().get(labels)
                        .setTpTotal(customFunctions.calculateWeigthHours(vEventTeachers.getDuree()));
                if(vEventTeachers.getDtStart().before(date)) {
                    totalStats.getInfos().get(labels)
                            .setTpDone((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));
                }

            default:
                totalStats.getInfos().get(labels)
                        .setCmTotal(customFunctions.calculateWeigthHours(vEventTeachers.getDuree()));
                if(vEventTeachers.getDtStart().before(date)) {
                    totalStats.getInfos().get(labels)
                            .setCmDone((customFunctions.calculateWeigthHours(vEventTeachers.getDuree())));
                }
        }

        return totalStats;
    }

    private Integer labelBuilder(CalendarEvents event) {

        return ( (event.getDtStart().getMonth() + 1) < 10) ? Integer.parseInt(
                event.getDtStart().toString().split(" ")[5]
                        + "0" + (event.getDtStart().getMonth() + 1) )
                : Integer.parseInt(
                event.getDtStart().toString().split(" ")[5]
                        + "" + (event.getDtStart().getMonth() + 1)) ;

    }
}
