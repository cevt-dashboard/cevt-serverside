package com.cevt.server.service;

import com.cevt.server.model.EventDescription;
import com.cevt.server.model.original.VEventGroupes;
import com.cevt.server.model.original.VEventTeachers;
import com.cevt.server.utils.ICSKeys;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/*
    Conversion from ICS to POJO
 */

@Component
@Slf4j
public class DataService extends AbstractService {

    public List<VEventTeachers> getTeachersEventsAsPojo(List<String> array, String calName) {

        List<VEventTeachers> eventList = new LinkedList<>();
        VEventTeachers e = new VEventTeachers();

        for(String val: array) {

            if(val.contains(ICSKeys.BEGIN.getDescription())) {
                e = new VEventTeachers();
                e.setCalName(calName);
            }

            if( val.contains(ICSKeys.SUMMARY.getDescription())) {
                e.setSummary(val.replaceAll(ICSKeys.SUMMARY.getDescription(), ""));
            }

            if(val.contains(ICSKeys.CATEGORIES.getDescription())) {
                e.setCategories(val.replaceAll(ICSKeys.CATEGORIES.getDescription(), ""));
            }

            if( val.contains(ICSKeys.DTSTART.getDescription()) ) {
                e.setDtStart( getDateFromString (val.replaceAll(ICSKeys.DTSTART.getDescription(), "")));
            }

            if(val.contains(ICSKeys.DTEND.getDescription())) {
                e.setDtEnd(getDateFromString (val.replaceAll(ICSKeys.DTEND.getDescription(), "")) );
            }

            if( val.contains(ICSKeys.LOCATION.getDescription())) {
                e.setLocation(val.replaceAll(ICSKeys.LOCATION.getDescription(), ""));
            }

            if( val.contains(ICSKeys.DESCRIPTION.getDescription())) {
                if( calName.contains("_")) {
                    e.setDescription(getTeacherDescription(val, e.getSummary() ));
                }else {
                    e.setDescription(getGroupeDescription(val, e.getSummary() ));
                }
            }

            if( val.contains(ICSKeys.DTSTAMP.getDescription())) {
                e.setDtStamp( getDateFromString (val.replaceAll(ICSKeys.DTSTAMP.getDescription(), "")) );
            }
            if(val.contains(ICSKeys.UID.getDescription())) {
                e.setUid(val.replaceAll(ICSKeys.UID.getDescription(), ""));
            }

            if(val.contains(ICSKeys.CALNAME.getDescription())) {
                e.setCalName(val.replaceAll(ICSKeys.CALNAME.getDescription(), "").toUpperCase() );
            }

            if(val.contains(ICSKeys.END.getDescription())) {
                eventList.add(e);
            }

        }

        return  eventList;

    }

    public List<VEventGroupes> getGroupesEventsAsPojo(List<String> array, String calName) {

        List<VEventGroupes> eventList = new ArrayList<>();
        VEventGroupes e = new VEventGroupes();
        EventDescription description = new EventDescription();
        for(String val: array) {

            if(val.contains(ICSKeys.BEGIN.getDescription())) {
                e = new VEventGroupes();
                e.setCalName(calName.toUpperCase());
            }

            if( val.contains(ICSKeys.SUMMARY.getDescription())) {
                e.setSummary(val.replaceAll(ICSKeys.SUMMARY.getDescription(), ""));
            }
            if( val.contains(ICSKeys.DTSTART.getDescription()) ) {
                e.setDtStart( getDateFromString (val.replaceAll(ICSKeys.DTSTART.getDescription(), "")));

            }

            if(val.contains(ICSKeys.DTEND.getDescription())) {
                e.setDtEnd(getDateFromString (val.replaceAll(ICSKeys.DTEND.getDescription(), "")) );
            }

            if( val.contains(ICSKeys.LOCATION.getDescription())) {
                e.setLocation(val.replaceAll(ICSKeys.LOCATION.getDescription(), ""));
            }

            if( val.contains(ICSKeys.DESCRIPTION.getDescription())) {

                if( calName.contains("_")) {
                    e.setDescription(getTeacherDescription(val, e.getSummary()));
                }else {
                    e.setDescription(getGroupeDescription(val, e.getSummary() ));
                }

            }

            if( val.contains(ICSKeys.DTSTAMP.getDescription())) {
                e.setDtStamp( getDateFromString (val.replaceAll(ICSKeys.DTSTAMP.getDescription(), "")) );
            }
            if(val.contains(ICSKeys.UID.getDescription())) {
                e.setUid(val.replaceAll(ICSKeys.UID.getDescription(), ""));
            }

            if(val.contains(ICSKeys.CALNAME.getDescription())) {
                e.setCalName(val.replaceAll(ICSKeys.CALNAME.getDescription(), "").toUpperCase() );
            }

            if(val.contains(ICSKeys.END.getDescription())) {
                eventList.add(e);
            }

        }

        return  eventList;

    }

}
