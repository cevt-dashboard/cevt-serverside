package com.cevt.server.service;

import com.cevt.server.model.auth.ERole;
import com.cevt.server.model.auth.Role;
import com.cevt.server.model.auth.User;
import com.cevt.server.repository.RoleRepository;
import com.cevt.server.repository.UserRepository;
import com.cevt.server.security.jwt.JwtUtils;
import com.cevt.server.security.payload.request.LoginRequest;
import com.cevt.server.security.payload.request.SignupRequest;
import com.cevt.server.security.payload.response.JwtResponse;
import com.cevt.server.security.payload.response.MessageResponse;
import com.cevt.server.security.services.UserDetailsImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class AuthService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtils jwtUtils;


    public ResponseEntity<?> authenticateUser( LoginRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);

        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());
        log.info("login user : "+ loginRequest.getUsername());

        return ResponseEntity.ok(new JwtResponse(jwt,
                userDetails.getId(),
                userDetails.getUsername(),
                userDetails.getLastName(),
                userDetails.getFirstName(),
                userDetails.getEmail(),
                roles));
    }


    public ResponseEntity<?> registerUser(SignupRequest signUpRequest, ERole eRole) {
        if (userRepository.existsByUsername(signUpRequest.getUsername())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.getEmail())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        // Create new user's account
        User user = new User(signUpRequest.getUsername(),
                signUpRequest.getLastName(),
                signUpRequest.getFirstName(),
                signUpRequest.getEmail(),
                encoder.encode(signUpRequest.getPassword()));

        Set<String> strRoles = signUpRequest.getRoles();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                   case "ROLE_ADMIN":
                        if(eRole.equals(ERole.ROLE_ADMIN)) {
                            Role adminRole = roleRepository.findByName(ERole.ROLE_ADMIN)
                                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                            roles.add(adminRole);
                        }


                        break;
                   case "ROLE_ENSEIGNANT":
                       if(eRole.equals(ERole.ROLE_ADMIN) || eRole.equals(ERole.ROLE_ENSEIGNANT) ) {
                           Role enseignantRole = roleRepository.findByName(ERole.ROLE_ENSEIGNANT)
                                   .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                           roles.add(enseignantRole);
                       }
                        break;
                   default:
                        Role userRole = roleRepository.findByName(ERole.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);
        log.info("creating  user : "+ signUpRequest.getUsername() );
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }


}
