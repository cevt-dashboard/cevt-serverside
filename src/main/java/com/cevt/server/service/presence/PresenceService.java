package com.cevt.server.service.presence;

import com.cevt.server.model.presence.Presence;
import com.cevt.server.repository.PresenceRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Random;

@Component
@Slf4j
public class PresenceService {

    @Autowired
    public PresenceRepository presenceRepository;

    public boolean update(String code, String name, String fingerprint) {
        Presence p = presenceRepository.findByCode(code);
        System.out.println(fingerprint);
        if( ("specialauthorisationprof".equals(fingerprint) || !p.getEtudiants().values().contains(fingerprint)) && !p.getIsClosed()  ) {
            p.setNewEtudiant(name, fingerprint);
            return presenceRepository.save(p) != null ;
        }

        return false;

    }

    public boolean close(String code) {
        Presence p = presenceRepository.findByCode(code);

        p.setClosed(true);
        return presenceRepository.save(p) != null ;
    }

    public Presence startWorkflow(String enseignant, String seanceId , String filiere) {

        Presence p = presenceRepository.save(Presence.builder()
                .creation_Date(new Date())
                .code(generateUniqueCode())
                .seanceId(seanceId)
                .filiere(filiere)
                .isClosed(false)
                .enseignant(enseignant).build());

        return p;
    }


    public String generateUniqueCode() {
        String newCode = null;
        int i = 0;
        do {
            newCode = getRandomNumberString();
            i++;
        }
        while( presenceRepository.existsByCode(newCode) && i < 10);
        return newCode;
    }

    private String getRandomNumberString() {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        Random rnd = new Random();
        int number = rnd.nextInt(999999);

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }

}
