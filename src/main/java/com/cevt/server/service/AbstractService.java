package com.cevt.server.service;

import com.cevt.server.model.EventDescription;
import com.cevt.server.utils.ICSKeys;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public abstract class AbstractService {
    private final static RestTemplate restTemplate = new RestTemplate();
    private final static DateTimeZone europeParis = DateTimeZone.forID("Europe/Paris");

    public List<String> getEventsAsArray(String fileName) throws IOException {
        File file = null;

        log.info("Request : "+ fileName);
        // https://edt.univ-evry.fr/icsprof/ivanov_sergiu.ics
        file = writeByte( restTemplate.exchange( fileName, HttpMethod.GET, null, byte[].class )
                    .getBody(), fileName.split("/")[4]);

        return convertIcsToArray(file);
    }

    protected EventDescription getTeacherDescription(String val, String Summary) {
        EventDescription eventDescription = new EventDescription();

        if( !val.contains("GROUPE :") ) {
            eventDescription.setGroupe("PUBLIC");

            String[] description = val.replaceAll(ICSKeys.DESCRIPTION.getDescription(), "")
                    .split("\\\\n");

            // TODO Fix WITH REGEX
            eventDescription.setDuree(description[1].replaceAll("DUREE :", "")
                    .replaceFirst(" ", ""));
            description = description[0].split(":");
            eventDescription.setLanguage(description[0].replaceAll("LANGUAGE=", ""));
            eventDescription.setMatiere(description[2].replaceFirst(" ", ""));
            eventDescription.setType("CM"); // TODO REMOVE THAT

            return eventDescription;
        }


        String[] description = val.replaceAll(ICSKeys.DESCRIPTION.getDescription(), "").split("\\\\n");

        // TODO Fix WITH REGEX
        eventDescription.setDuree(description[2].replaceAll("DUREE :", "").replaceFirst(" ", ""));
        eventDescription.setGroupe(description[1].replaceAll("GROUPE :", "").replaceFirst(" ", ""));
        description = description[0].split(":");
        eventDescription.setLanguage(description[0].replaceAll("LANGUAGE=", ""));
        eventDescription.setMatiere(description[2].split("-")[0].replaceFirst(" ", ""));
        eventDescription.setType(description[2].split("-")[1].replaceFirst(" ", ""));

        return  eventDescription;
    }

    protected EventDescription getGroupeDescription(String val, String summary) {
        EventDescription eventDescription = new EventDescription();

        if( !val.contains("GROUPE :") ) {
            eventDescription.setGroupe("PUBLIC");

            String[] description = val.replaceAll(ICSKeys.DESCRIPTION.getDescription(), "")
                    .split("\\\\n");

            if ( description.length > 2 ) {
                eventDescription.setDuree(description[2].replaceAll("DUREE :", "")
                        .replaceFirst(" ", ""));
                eventDescription.setGroupe(description[1].replaceAll("PROF :", "").replaceFirst(" ", ""));

            } else {
                eventDescription.setDuree(description[1].replaceAll("DUREE :", "")
                        .replaceFirst(" ", ""));
            }

            // TODO Fix WITH REGEX

            description = description[0].split(":");
            eventDescription.setLanguage(description[0].replaceAll("LANGUAGE=", ""));
            eventDescription.setMatiere(description[2].replaceFirst(" ", ""));

            if( summary != null) {

                if( summary.toUpperCase().contains("TD") )
                    eventDescription.setType("TD");
                if ( summary.toUpperCase().contains("TP"))
                    eventDescription.setType("TP");
                if ( summary.toUpperCase().contains("EXAMEN"))
                    eventDescription.setType("EXAMEN");
                if ( summary.toUpperCase().contains("CM") )
                    eventDescription.setType("CM");
            }

            if( eventDescription.getType() == null ) {
                eventDescription.setType("AUTRE");
            }



            return eventDescription;
        }


        String[] description = val.replaceAll(ICSKeys.DESCRIPTION.getDescription(), "").split("\\\\n");

        // TODO Fix WITH REGEX
        eventDescription.setDuree(description[2].replaceAll("DUREE :", "").replaceFirst(" ", ""));
        eventDescription.setGroupe(description[1].replaceAll("GROUPE :", "").replaceFirst(" ", ""));
        description = description[0].split(":");
        eventDescription.setLanguage(description[0].replaceAll("LANGUAGE=", ""));
        eventDescription.setMatiere(description[2].split("-")[0].replaceFirst(" ", ""));
        eventDescription.setType(description[2].split("-")[1].replaceFirst(" ", ""));

        return  eventDescription;
    }

    protected File writeByte(byte[] bytes, String fileName) throws IOException {

        File file = null;
        try {
            file = new File( fileName);
            OutputStream os = new FileOutputStream(file);
            os.write(bytes);
            os.close();
        }

        catch (Exception e) {
            throw e;
        }

        return file;
    }

    protected Date getDateFromString(String dateInString) {

        try {
            dateInString = dateInString.replaceAll("Z", "");
            dateInString = dateInString.replaceAll("T", "");
            char[] dateInStringArray = dateInString.toCharArray();

            String formatedDate =
                    ""+dateInStringArray[0] + dateInStringArray[1] + dateInStringArray[2] + dateInStringArray[3]+
                            "/" + dateInStringArray[4] + dateInStringArray[5] +
                            "/" + dateInStringArray[6] + dateInStringArray[7] +
                            " " + dateInStringArray[8] + dateInStringArray[9] +
                            ":" + dateInStringArray[10] + dateInStringArray[11] +
                            ":" + dateInStringArray[12] + dateInStringArray[13];

            return DateTime
                    .parse(formatedDate, DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss").withZone(europeParis)).toDate() ;

        }catch (Exception e){}

        return null;
    }

    private List<String> convertIcsToArray(File file) throws IOException {
        BufferedReader reader = null;
        try {
            FileReader fichier = new FileReader( file );
            reader = new BufferedReader(fichier);
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        String myLine = "";
        List<String> array = new ArrayList<>();
        while ((myLine = reader.readLine()) != null ) {
            array.add(myLine);
        }

        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        file.delete();
        return array;
    }

    protected String trimMatiereToBeConform(String matiereRaw) {
        return matiereRaw.replaceAll("[()]", "").trim();

    }

    protected String filterFiliereToBeConform(String filiere) {


        for(ValueToTrimFromFiliereName s: ValueToTrimFromFiliereName.values()) {
            if(filiere.contains(s.getWordToDelimit())) {
                filiere = filiere.split(s.getWordToDelimit())[0];
            }
        }


        return filiere;
    }
}
@Getter
enum ValueToTrimFromFiliereName {

    FI("-FI-"),
    FA("-FA"),
    FAA("-FA-"),
    DEROG(" DEROG "),
    FIA(" FIA "),
    CM("-CM-"),
    EXAMEN("-EXAMEN"),
    APPRENTISSAGE("-APPRENTISSAGE"),
    CRYPTOGRAPHIE("-CRYPTOGRAPHIE"),
    GRIGRA("-GRIGRA"),
    INFOHP("-INFOHP"),
    METHODES("-METHODES "),
    MODELES("-MODELES "),
    PROJET("-PROJET "),
    PROTOCOLES("-PROTOCOLES "),
    RESVAN("-RESVAN"),
    SIMULATION("-SIMULATION "),
    SPECIFICATION("-SPECIFICATION"),
    SYSTEMES("-SYSTEMES "),
    PROCESSUS("-PROCESSUS "),
    TD("-TD-"),
    ONE("-111"),
    MINUSONE(" - 111"),
    ANALYSE("-ANALYSE"),
    IMPLEMENTATION("-IMPLEMENTATION"),
    ADMINISTRATION("-ADMINISTRATION");

    private String wordToDelimit;

    ValueToTrimFromFiliereName(String wordToDelimit) {
        this.wordToDelimit = wordToDelimit;
    }



}
