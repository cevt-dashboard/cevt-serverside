package com.cevt.server.service.publication;

import com.cevt.server.model.CalendarEvents;
import com.cevt.server.repository.CalendarEventsRepository;
import com.cevt.server.service.CalendarEventsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@EnableScheduling
@Slf4j
public class SchedulerService {
    // https://stackoverflow.com/questions/45124756/spring-scheduling-cron-expression-for-everyday-at-midnight-not-working

    @Autowired
    private CalendarEventsRepository calendarEventsRepository;

    @Autowired
    private CalendarEventsService calendarEventsService;

    @Autowired
    private SnitchService snitchService;

    @Scheduled(cron = "0 0 18 * * *", zone = "Europe/Paris")
    public Object notifyGroupEventsChanges() {

        System.out.println("Scheduled task running");

        List<CalendarEvents> new_List = calendarEventsService.dumpTeachersData(true);

        for(CalendarEvents event: calendarEventsRepository.findAll()) {

        }

        return null;
    }


   // @Scheduled(cron = "0 0 19 * * *", zone = "Europe/Paris")
    public void refreshTeachersEventsChanges() {

        System.out.println("Scheduled TEACHER task running");

        // Map<String, List<VEventTeachers>> map = adminService.dumpTeachersData();

        // log.info("Refresh Teachers Data done : " + map.keySet().size() );
    }
}
