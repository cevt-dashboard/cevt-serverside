package com.cevt.server.service.publication;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;


/*
    This service will snitch to twitter a list of groupes that have to check their edt
 */
@Component
@Slf4j
public class SnitchService {

    @Autowired
    private Twitter twitter;


// https://github.com/eugenp/tutorials/blob/433df90ea50285f4065b5afeaf094b7cd6c0e2be/twitter4j/src/main/java/com/baeldung/Application.java#L25
    public Status tweet(String tweetText) {
        try {


            return twitter.updateStatus(tweetText);

        } catch (RuntimeException | TwitterException ex) {
            log.error("Unable to tweet" + tweetText, ex);
        }
        return null;
    }

}
