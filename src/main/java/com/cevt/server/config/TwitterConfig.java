package com.cevt.server.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import twitter4j.Twitter;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

@Configuration
public class TwitterConfig {

    @Autowired
    private Environment env;

    @Bean
    public Twitter getTwtTemplate() {
        String consumerKey = env.getProperty(".SpringAtSO.consumerKey");
        String consumerSecret = env.getProperty(".SpringAtSO.consumerSecret");
        String accessToken = env.getProperty(".SpringAtSO.accessToken");
        String accessTokenSecret = env.getProperty(".SpringAtSO.accessTokenSecret");

        ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setDebugEnabled(true)
		  .setOAuthConsumerKey(consumerKey)
		  .setOAuthConsumerSecret(consumerSecret)
	      .setOAuthAccessToken(accessToken)
	      .setOAuthAccessTokenSecret(accessTokenSecret);
		TwitterFactory tf = new TwitterFactory(cb.build());
		return tf.getSingleton();
    }
}
