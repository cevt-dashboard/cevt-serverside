package com.cevt.server.repository;

import com.cevt.server.model.DenominationMatiere;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DenominationMatiereRepository extends MongoRepository<DenominationMatiere, String> {

}
