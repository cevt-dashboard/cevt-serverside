package com.cevt.server.repository;

import com.cevt.server.model.NotificationChangeEvent;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NotificationChanceEventsRepository extends MongoRepository<NotificationChangeEvent, String> {
}
