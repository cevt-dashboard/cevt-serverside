package com.cevt.server.repository;

import com.cevt.server.model.presence.Presence;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PresenceRepository  extends MongoRepository<Presence, String>  {
    Boolean existsByCode(String code);
    Presence findByCode(String code);
    List<Presence> findByEnseignant(String enseignant);
    Presence findByCodeAndEnseignant(String code, String enseignant);
}
