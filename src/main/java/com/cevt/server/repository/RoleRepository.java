package com.cevt.server.repository;

import com.cevt.server.model.auth.ERole;
import com.cevt.server.model.auth.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);

    @Override
    <S extends Role> S save(S s);
}

