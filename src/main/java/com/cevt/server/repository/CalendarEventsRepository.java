package com.cevt.server.repository;

import com.cevt.server.model.CalendarEvents;
import com.cevt.server.model.EventType;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface CalendarEventsRepository extends MongoRepository<CalendarEvents, String> {

    List<CalendarEvents> findAllByProfIgnoreCaseAndValid(String prof, EventType event);
    List<CalendarEvents> findAllByValid(EventType event);
    List<CalendarEvents> findAllByValidAndCustom(EventType event, EventType custom );

    List<CalendarEvents> findAllByCategoriesContainingIgnoreCase(String categories);
    List<CalendarEvents> findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndProfAndValid(Date dtStart,
                                                                                               Date dtEnd,
                                                                                               String calName,
                                                                                               EventType event);
    List<CalendarEvents> findByDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndValid(Date dtStart, Date dtEnd, EventType event);
    List<CalendarEvents> findAllByProfAndCategoriesContainingIgnoreCaseAndDtStartIsGreaterThanEqualAndDtEndIsLessThanEqualAndValid
            (String prof, String categories, Date dtStart, Date dtEnd, EventType event );

    List<CalendarEvents> findAllByCreator(String creator);
    List<CalendarEvents> findAllByCustom(EventType custom);
    long deleteAllByCustom(EventType custom);
    long deleteAllById(String id);
 }
