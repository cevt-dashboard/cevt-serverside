package com.cevt.server.repository;

import com.cevt.server.model.GroupNames;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GroupNamesRepository extends MongoRepository<GroupNames, String> {
    Boolean existsByName(String name);
    Long deleteByName(String name);
    GroupNames findByName(String name);
}
