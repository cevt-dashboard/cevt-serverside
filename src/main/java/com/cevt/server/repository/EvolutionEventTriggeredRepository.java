package com.cevt.server.repository;


import com.cevt.server.model.EvolutionEventTriggered;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface EvolutionEventTriggeredRepository extends MongoRepository<EvolutionEventTriggered, String> {

    List<EvolutionEventTriggered> findByDate(String date);
}
