package com.cevt.server.model.original;

import com.cevt.server.model.EventDescription;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class VEventGroupes {

    @Id
    private String id;

    private String summary;
    private String calName;
    private Date dtStart;
    private Date dtEnd;
    private String location;
    private EventDescription description;
    private Date dtStamp;
    private String uid;


}
