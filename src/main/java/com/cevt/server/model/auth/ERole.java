package com.cevt.server.model.auth;

public enum ERole {
    ROLE_USER,
    ROLE_ENSEIGNANT,
    ROLE_ADMIN,
}

