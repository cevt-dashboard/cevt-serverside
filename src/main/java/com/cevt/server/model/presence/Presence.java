package com.cevt.server.model.presence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "PresenceEvents")
public class Presence {
    @Id
    private String id;

    private String seanceId;
    private String filiere;
    private String enseignant;
    private String code;
    private Date creation_Date;
    private Map<String, String> etudiants = new HashMap<>();
    private boolean isClosed;

    public boolean getIsClosed() {
        return this.isClosed;
    }
    public void setNewEtudiant(String etu, String fingerPrint) {
        this.etudiants.put(etu, fingerPrint);
    }


}
