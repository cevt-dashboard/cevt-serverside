package com.cevt.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EventDescription {

    private String language;
    private String matiere;
    private String type;
    private String groupe;
    private String duree;

}

