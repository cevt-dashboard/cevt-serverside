package com.cevt.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "EvolutionEventTriggered")
public class EvolutionEventTriggered {

    @Id
    private String id;

    private List<CalendarEvents> oldEvent = new ArrayList<>();
    private List<CalendarEvents> newEvent = new ArrayList<>();


    private String date;

    public EvolutionEventTriggered(List<CalendarEvents> oldEvent, List<CalendarEvents> newEvent, String date) {
        this.oldEvent = oldEvent;
        this.newEvent = newEvent;
        this.date = date;
    }
}
