package com.cevt.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "groupNames")
@Builder
public class GroupNames {

    private String name;
    private String link;
    private List<String> variants = new ArrayList<>();
    private EventType custom;
}
