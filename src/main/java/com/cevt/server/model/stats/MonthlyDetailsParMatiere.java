package com.cevt.server.model.stats;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MonthlyDetailsParMatiere extends MonthlyDetails {
    protected Date date_debut;
    protected Date date_fin;


}
