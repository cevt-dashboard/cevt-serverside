package com.cevt.server.model.stats;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.LinkedHashMap;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TotalStats {
    private String calName;
    private Map<Integer, MonthlyDetails> infos = new LinkedHashMap<>();


}


