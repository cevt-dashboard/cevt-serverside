package com.cevt.server.model.stats;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MonthlyDetails  {
    protected double cmTotal;
    protected double cmDone;
    protected double tdTotal;
    protected double tdDone;
    protected double tpTotal;
    protected double tpDone;


    public void setCmTotal(double cmTotal) {
        this.cmTotal += cmTotal;
    }

    public void setCmDone(double cmDone) {
        this.cmDone += cmDone;
    }

    public void setTdTotal(double tdTotal) {
        this.tdTotal += tdTotal;
    }

    public void setTdDone(double tdDone) {
        this.tdDone += tdDone;
    }

    public void setTpTotal(double tpTotal) {
        this.tpTotal += tpTotal;
    }

    public void setTpDone(double tpDone) {
        this.tpDone += tpDone;
    }


    public void setAll(MonthlyDetails m) {
        this.setTdTotal(m.getTdTotal());
        this.setTpTotal(m.getTpTotal());
        this.setCmTotal(m.getCmTotal());

        this.setTdDone(m.getTdDone());
        this.setTpDone(m.getTpDone());
        this.setCmDone(m.getCmDone());
    }
}
