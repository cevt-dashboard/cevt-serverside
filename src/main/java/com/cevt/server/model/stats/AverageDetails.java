package com.cevt.server.model.stats;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;



@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AverageDetails {

    private String label;
    private String measures;
    private double total;
    private double average;


}
