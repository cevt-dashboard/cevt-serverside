package com.cevt.server.model.stats;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Statsdata{
    double cm = 0;
    double tp = 0;
    double td = 0;

    public void add(Statsdata data) {
        this.setCm(data.getCm());
        this.setTd(data.getTd());
        this.setTp(data.getTp());
    }
    public void setCm(double cm) {
        this.cm += cm;
    }
    public void setTd(double td) {
        this.td += td;
    }
    public void setTp(double tp) {
        this.tp += tp;
    }
}
