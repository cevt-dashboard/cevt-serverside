package com.cevt.server.model.stats;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TotalPerMonth {
    private Map<Integer, MonthlyDetails> map;
    private Set<String> dataProviders;
}
