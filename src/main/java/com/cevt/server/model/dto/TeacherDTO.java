package com.cevt.server.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TeacherDTO {

    private String lastName;
    private String firstName;
    private String email;
    private String status;
    private String service;
    private String department;
    private String composante;
}
