package com.cevt.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "NotificationsEvents")
@Builder
public class NotificationChangeEvent {

    private CalendarEvents old;
    private CalendarEvents new_version;
}
