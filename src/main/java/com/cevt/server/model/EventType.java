package com.cevt.server.model;

public enum EventType {
    YES,
    NO,
    REFUSED
}
