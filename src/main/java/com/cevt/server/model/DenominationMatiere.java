package com.cevt.server.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "denominationMatiere")

public class DenominationMatiere {

    @Id
    private String id;

    private String nom_complet;
    private String abbreviation;
}
