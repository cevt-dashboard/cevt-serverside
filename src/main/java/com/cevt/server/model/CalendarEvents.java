package com.cevt.server.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "CalendarEvents")
public class CalendarEvents {
    @Id
    private String id;

    private String prof;
    private String summary;
    private String categories;
    private Date dtStart;
    private Date dtEnd;
    private String location;

    private String language;
    private String matiere;
    private String type;
    private String groupe;
    private String duree;

    private Date dtStamp;
    private String uid;
    private EventType custom;
    private EventType valid;
    private String creator;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CalendarEvents)) return false;
        CalendarEvents events = (CalendarEvents) o;
        return Objects.equals(getProf(), events.getProf()) &&
                Objects.equals(getSummary(), events.getSummary()) &&
                Objects.equals(getCategories(), events.getCategories()) &&
                Objects.equals(getLanguage(), events.getLanguage()) &&
                Objects.equals(getMatiere(), events.getMatiere()) &&
                Objects.equals(getType(), events.getType()) &&
                Objects.equals(getUid(), events.getUid()) &&
                Objects.equals(getGroupe(), events.getGroupe()) &&
                getCustom() == events.getCustom() &&
                getValid() == events.getValid() ;
    }


    public boolean hasChanged(CalendarEvents newEvent) {
       // return !this.duree.equals(newEvent.duree);
        return this.getDtStart().compareTo(newEvent.getDtStart()) != 0
                ||  this.getDtEnd().compareTo(newEvent.getDtEnd()) != 0
                || !this.getLocation().equals(newEvent.getLocation())
                || !this.duree.equals(newEvent.duree)

                ;
    }

    @Override
    public String toString() {
        return "CalendarEvents{" +
                ", prof='" + prof + '\'' +
                ", summary='" + summary + '\'' +
                ", categories='" + categories + '\'' +
                ", dtStart=" + dtStart +
                ", dtEnd=" + dtEnd +
                ", location='" + location + '\'' +
                ", language='" + language + '\'' +
                ", matiere='" + matiere + '\'' +
                ", type='" + type + '\'' +
                ", groupe='" + groupe + '\'' +
                ", duree='" + duree + '\'' +
                ", dtStamp=" + dtStamp +
                ", uid='" + uid + '\'' +
                ", custom=" + custom +
                ", valid=" + valid +
                ", creator='" + creator + '\'' +
                '}';
    }

}
