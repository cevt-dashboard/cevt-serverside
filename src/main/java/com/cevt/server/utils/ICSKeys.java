package com.cevt.server.utils;

public enum ICSKeys {
    BEGINVCALENDAR("BEGIN:VCALENDAR"),
    VERSION("VERSION:"),
    PRODID("PRODID:"),
    METHOD("METHOD:"),
    CALSCALE("CALSCALE:"),
    CALNAME("X-WR-CALNAME:"),
    TIMEZONE("X-WR-TIMEZONE:"),
    BEGIN("BEGIN:VEVENT"),
    SUMMARY("SUMMARY:"),
    CATEGORIES("CATEGORIES:"),
    DTSTART("DTSTART:"),
    DTEND("DTEND:"),
    LOCATION("LOCATION:"),
    DESCRIPTION("DESCRIPTION;"),
    DTSTAMP("DTSTAMP:"),
    UID("UID:"),
    END("END:VEVENT");



    private String description;

    ICSKeys(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
