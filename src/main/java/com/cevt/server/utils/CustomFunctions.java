package com.cevt.server.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.Date;

@Component
@Slf4j
public class CustomFunctions {
    @Value("${vtAgenda.url}")
    public String vtAgenda;

    private final SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
    private final int year = LocalDate.now().getYear();

    public String getTeachersCalName(String firstName, String lastName) {
        return firstName.toUpperCase().replaceAll(" ", "_") + " " + lastName.toUpperCase().replaceAll(" ", "_");
    }

    public String getTeachersURL(String calName) {

        return vtAgenda + "/icsprof/" +StringUtils.stripAccents(
                calName.replaceAll(" ", "_").toLowerCase()) + ".ics";
    }

    public  AbstractMap.SimpleEntry<Date, Date> getCoupleDateStartAndEnd() throws ParseException {

        if( new Date().after(formatter2.parse(year + "-07-15"))) {
            return new AbstractMap.SimpleEntry<>(formatter2.parse(String.valueOf(year  ) + "-08-24"),
                    formatter2.parse((year +1) + "-07-15")) ;
        }
        return new AbstractMap.SimpleEntry<>(formatter2.parse(String.valueOf(year -1 ) + "-08-24"),
                formatter2.parse(year + "-07-15")) ;

    }

    public  double calculateWeigthHours(String duree) {
        switch (duree) {
            case "1h00":
                return 1;
            case "1h15":
                return 1.25;
            case "1h30":
                return 1.5;
            case "1h45":
                return 1.75;

            case "2h00":
                return 2;
            case "2h15":
                return 2.25;
            case "2h30":
                return 2.5;
            case "2h45":
                return 2.75;

            case "3h00":
                return 3;
            case "3h15":
                return 3.25;
            case "3h30":
                return 3.5;
            case "3h45":
                return 3.75;

            case "4h00":
                return 4;
            case "4h15":
                return 4.25;
            case "4h30":
                return 4.5;
            case "4h45":
                return 4.75;

            case "5h00":
                return 5;
            case "5h15":
                return 5.25;
            case "5h30":
                return 5.5;
            case "5h45":
                return 5.75;

            case "6h00":
                return 6;
            case "6h15":
                return 6.25;
            case "6h30":
                return 6.5;
            case "6h45":
                return 6.75;

            case "7h00":
                return 7;
            case "7h30":
                return 7.5;

            case "8h00":
                return 8;
            case "8h30":
                return 8.5;

            case "9h00":
                return 9;
            case "9h30":
                return 9.5;

            case "10h00":
                return 10;
            case "10h30":
                return 10.5;

            default:
                return 0;
        }
    }

}
