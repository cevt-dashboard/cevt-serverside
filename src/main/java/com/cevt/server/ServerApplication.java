package com.cevt.server;

import com.cevt.server.model.auth.ERole;
import com.cevt.server.model.auth.Role;
import com.cevt.server.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(ServerApplication.class, args);
	}

	@Autowired
	RoleRepository roleRepository;


	@Override
	public void run(String... args) {

		Role role1 = new Role();
		role1.setId("1");
		role1.setName(ERole.ROLE_USER);
		roleRepository.save(role1);


		Role role2 = new Role();
		role2.setId("2");
		role2.setName(ERole.ROLE_ENSEIGNANT);
		roleRepository.save(role2);

		Role role3 = new Role();
		role3.setId("3");
		role3.setName(ERole.ROLE_ADMIN);
		roleRepository.save(role3);

	}

}


