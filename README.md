 Projet Developpement Logiciel [![License: CC BY-NC-SA 4.0](https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg)](http://creativecommons.org/licenses/by-nc-sa/4.0/) 
========
 
Nom du Projet : CEVT-Dashboard 

#### Université Évry - Projet #DLL8

Une application open source offrant aux enseignants-chercheurs la possibilité de connaitre en temps réel leur charges d'enseignements. 

### OBJECTIFS 
* La <b> mise en place d'une API Rest </b> :
    - Ce serveur doit pouvoir fournir des statistiques détaillées en fonction de chaque enseignants-chercheurs.
    - Accès à la récupération d'EdT au format ICS par login/mdp
    - Un Système d'authentification.
    - Une sauvegarde des données dans une Base de donnée Mongo.
    
* Un <b>Client Web </b>:
    - Module graphiques
    - Module Calendrier moderne
    - Module Administrateur ( CRUD enseignants + groupes )
 
## Référencement des Ressources (endpoints)


### Data  

*  <b> /data/ics-to-json/{first name}/{last name} </b> :
    - Permet d'importer les évènements en format Json d'un enseignant. L'option " save " permet de sauvegarder les 
    évènements dans la base de donnée MongoDB.


*  <b> /data/events-raw/{groupName} </b> :
    - Permet de récupérer et lire le fichier ICS originel d'un groupe depuis Vtagenda ( List<String> ).


*  <b> /data/raw-array/{firstName}/{lastName} </b> : 
    - Permet de récupérer et lire l'ICS d'un professeur (List<String> ).  


*  <b> /data/groupes-json/{groupName} </b> : 
    - Permet de récupérer les évènements d'un groupe en format JSON. 


### Statistiques 

Les statistiques suivantes sont calculées en fonction de l'année universitaire : `[YEAR / 08 / 24 , Year+-1 / 07 / 14 ]`.
Par défaut, une IA ( :wink: ) calcule automatiquement l'Année universitaire courante.
Afin de preciser une période personnalisée, veuillez renseigner les champs start_Date et end_Date en format yyyy-MM-dd 


*  <b> /stats/teacher/events-grouped-by-categories  </b> :
    - Permet a un enseignant de récupérer ses évènements agrégé en fonction des matières.           


*  <b> /stats/teacher/stats-by-group/{first name} / {last name} / {Group Name} </b> :
    - Permet a un enseignant de récupérer le Total de la charge d'enseignement qu'il DEVRAIT effectuer durant l'A.U. 
    courante pour une classe/group spécifique.          


*  <b> /stats/annual-averages  </b> :
    - Permet de récupérer le total des heures ainsi que la moyenne de tout les professeurs actif.       


*  <b> /stats/monthly-total </b> :
    - Permet de récupérer la somme des CM/TD/TP en fonction des mois (Done = fait et Total = prévu ).          
 

*  <b> /stats/teacher/detail/ {first name} / {last name}  </b> :
    - Permet a un enseignant de récupérer le Total de la charge d'enseignement qu'il DEVRAI effectuer durant l'A.U. courante.          


### Calendar 

*  <b> /calendar/events-grouped-by-teacher/ {groupName}  </b> :
    - Permet de récupérer les évènements agrégé par le nom de l'enseignant          


*  <b> /calendar/teachers-events/ {firstName} / {lastName}  </b> :
    - Permet de récupérer les évènements de l'enseignant    
    
    
*  <b> /calendar/events-by-group/ {groupName}   </b> :
    - Permet de récupérer les évènements d'un groupe
    
    
##### Autre 

Un CRUD Enseignants + Groupe est ainsi disponible, pour l'usage de l'administrateur uniquement. 
L'administrateur a plusieurs endpoints supplémentaire pour configurer sa base de donnée.


### Informations

Pour toutes informations supplémentaires, veuillez consulter notre <a href="https://gitlab.com/groupNames/cevt-dashboard">Git</a>.


### Système de notification 

Afin de suivre les changements du calendrier vtAgenda, un bot Twitter permet de notifier les classes qui auront besoin de verifier leur edt a 18h.

<a href="https://twitter.com/CevtP">Lien</a>   


### Bugs & Problèmes
Pour signaler un soucis ou autres informations, veuillez nous adresser un ticket via le menu issues 
suivant : [issues](https://gitlab.com/groupNames/cevt-dashboard/-/issues).




## Installation

### Clone repo

```bash
# clone the repo
$ git clone <repoLink>

# go into app's directory
$ cd <project_name>

# install app's dependencies
$ ./gradlew bootRun   or ( $ npm install && npm start  for web client)
```

## Usage Intellij

```bash
# step 1
$ Open One of the project in intellij

# step 2
$ file > Project structure > Modules > + > import module

# step 3
$ now you can run the all api inside one intellij instance

```

## Prerequisites

- Intellij / vscode + Mongodb
- Java (jdk 11 ) + Gradle + node/npm
- Postman + ModHeader + Prettier plugin

## Swagger Links

- server : http://localhost:9090/swagger-ui.html#



## What's included in the api

```
Server/
├── gradle/
├── src/
│   ├── main/
│      ├── java/com/dystolab/userService
│           ├── config/
│           ├── model/
│           ├── repository/
│           ├── service/
│           ├── utils/
│           ├── web/
│           ├── ServerApplication.java
├── build.gradle
├── gradlew
├──  ...
└── settings.gradle
```


####Connect to mongocompass

// mongodb://USER:PASSWORD@IP:27017/dbName?authSource=dbName&readPreference=primary&gssapiServiceName=mongodb&appname=MongoDB%20Compass&ssl=false
